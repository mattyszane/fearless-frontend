window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      let loadingTag = document.getElementById("loading-conference-spinner");
      loadingTag.classList.add('d-none');
      selectTag.classList.remove('d-none');

    }

  });



  window.addEventListener('DOMContentLoaded', async () => {

    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const parsedJson = JSON.parse(json);
      console.log(parsedJson)
      const conferenceId = parsedJson["conference"];
      const attendeeUrl = `http://localhost:8001${conferenceId}attendees/`;
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(attendeeUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newattendee = await response.json();
        const successTag = document.getElementById("success-message");
        formTag.classList.add('d-none');
        successTag.classList.remove('d-none');
      }


    });
  });
