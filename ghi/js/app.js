function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col-md-4 shadow-lg p-3 mb-5 bg-body-tertiary rounded">
      <div class="card" margin: 30px; style="margin-bottom: 30px;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${start} - ${end}
        </div>
      </div>
    </div>
    `;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        showError("Error fetching data")

      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toLocaleDateString();
            const end = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, start, end, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      showError("Error Processing data")
    }

  });


  function showError(message){
    const alertHTML = `
    <div class="alert alert-primary" role="alert">
        ${message}
    </div>
    `

    document.body.innerHTML += alertHTML
  }
