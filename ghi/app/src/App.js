import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeeForm from "./AttendeeForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import {BrowserRouter, Routes, Route } from "react-router-dom"
function App(props) {
  const attendees = props.attendees;
  return (
    <>
    <div className="container">
      <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="/conferences/new" element={<ConferenceForm/>} ></Route>
        <Route path="/attendees/new" element={<AttendeeForm />} ></Route>
        <Route path="/locations/new" element={<LocationForm/>} ></Route>
        <Route path="/attendees" element={<AttendeesList attendees={attendees}/>} ></Route>
        <Route path="/presentations/new" element={<PresentationForm />}></Route>

      </Routes>
      </ BrowserRouter>
    </div>

    </>
  );
}

export default App;
